import 'package:flutter/material.dart';
import 'package:videoclub/Data/AppState.dart';
import 'package:videoclub/Data/Movie.dart';
import 'package:videoclub/Utils/api.dart';

class MovieScreen extends StatefulWidget {

  Movie movie;
  AppState appState;
  String callerPage;

  MovieScreen({
    @required this.movie,
    @required this.appState,
    @required this.callerPage
  }) : assert(movie != null),
       assert(appState != null),
       assert(callerPage != null);

  @override
  _MovieScreenState createState() => _MovieScreenState();
}


class _MovieScreenState extends State<MovieScreen> {

  bool favorite = false;


  @override
  void initState() {
    super.initState();
    favorite = widget.appState.isFavorite(widget.movie.id);
  }


  void onFavorite() {
    setState(() {
      favorite = !favorite;
      if (favorite)
        widget.appState.addFavorite(widget.movie.id);
      else
        widget.appState.removeFavorite(widget.movie.id);
    });
  }


  List<Widget> rating(double rating) {
    var star = new Icon(Icons.star);
    var halfStar = new Icon(Icons.star_half);
    var emptyStar = new Icon(Icons.star_border);
    double fiveStarsRating = (rating * 5) / 10;
    List<Widget> stars = [];

    for (int i = 0; i <= fiveStarsRating - 1; i++) stars.add(star);

    if (rating > stars.length - 0.5) stars.add(halfStar);

    for (int i = stars.length; i < 5; i++) stars.add(emptyStar);

      return stars;
  }


  List<Widget> _movieContainer() {
    return <Widget>[
      new SizedBox(height: 130.0,),
      new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Hero(
            tag: "${widget.callerPage}${widget.movie.id}",
            child: new Container(
              height: 200.0,
              width: 150.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: NetworkImage(
                      ApiMovie.movieImgUrl(widget.movie.posterPath)
                    ),
                    fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          new Row(
            children: rating(widget.movie.voteAverage),
          ),
        ],
      ),
      new SizedBox(height: 20.0,),
      new Text(widget.movie.title,
        style: Theme.of(context).textTheme.headline,
      ),
      new SizedBox(height: 20.0),
      new Text(widget.movie.overView,
        style: Theme.of(context).textTheme.body1.copyWith(
          fontSize: 16.0,
          color: Colors.grey[600],
        ),
      ),
      new SizedBox(height: 30.0,),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new SafeArea(
          child: new Stack(
            children: <Widget>[
              new SingleChildScrollView(
                child: Stack(
                  children: <Widget>[
                    new Container(
                      height: 220.0,
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                            image: NetworkImage(ApiMovie.movieImgUrl(
                                widget.movie.backdropPath
                            )),
                            fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    new Container(
                      height: 220.0,
                      decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [Colors.transparent, Color(0xFF000000)],
                        ),
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: _movieContainer(),
                      ),
                    ),
                 ],
                ),
              ),
              new Container(
                height: 60.0,
                child: new AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  brightness: Brightness.light,
                ),
              ),
            ],
          ),
      ),
      floatingActionButton: new IconButton(
          icon: favorite ? new Icon(Icons.favorite) : new Icon(Icons.favorite_border),
          iconSize: 35.0,
          onPressed: onFavorite,
      ),
    );
  }
}
