import 'package:flutter/material.dart';
import 'package:videoclub/Data/AppState.dart';
import 'package:videoclub/Data/Movie.dart';
import 'package:videoclub/Utils/api.dart';
import 'package:videoclub/Widgets/MovieCard.dart';

class PopularPage extends StatefulWidget {

  AppState appState = new AppState();

  PopularPage({@required this.appState}) : assert(appState != null);

  @override
  _PopularPageState createState() => _PopularPageState();
}

class _PopularPageState extends State<PopularPage> with
      AutomaticKeepAliveClientMixin<PopularPage>{

  @override
  void initState() {
    super.initState();
    if (widget.appState.movies.isEmpty) getMoreItems();
  }


  @override
  Widget build(BuildContext context) {
    if (widget.appState.movies.isEmpty)
      return Center(child: new CircularProgressIndicator(),);
    else
      return ListView.builder(
          itemBuilder: (context, index) {
            return _createList(index);
          },
          itemCount: widget.appState.movies.length + 1,
      );
  }

  Widget _createList(int index) {
    if (index < widget.appState.movies.length) {
      return new MovieCard(
        movie: widget.appState.movies[index],
        callerPage: 'popular',
      );
    } else {
      getMoreItems();
      return new CircularProgressIndicator();
    }
  }


  getMoreItems() async {
    List<Movie> movies = await ApiMovie.getMovies(++widget.appState.lastpage);
    widget.appState.movies.addAll(movies);
    setState(() {});
  }

  @override
  bool get wantKeepAlive => true;
}
