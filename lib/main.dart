import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:videoclub/Data/AppState.dart';
import 'package:videoclub/Data/AppStateProvider.dart';
import 'package:videoclub/Screens/favorite_screen.dart';
import 'package:videoclub/Screens/popular_screen.dart';

void main() async {
  runApp(AppStateProvider(
      appState: new AppState(),
      child: new MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    if (Platform.isAndroid) {
      return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Colors.blueGrey[600],
        ),
        home: MyHomePage(title: 'Video Club'),
        debugShowCheckedModeBanner: false,
      );
    } else {
      return CupertinoApp(
          title: 'Flutter Demo',
          theme: CupertinoThemeData(
            primaryColor: Colors.blueGrey[600],
          ),
          home: MyHomePage(title: 'Video Club'),
        debugShowCheckedModeBanner: false,
      );
    }

  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  PageController _pageController = new PageController();
  int _activePage = 0;


  @override
  void initState() {
    super.initState();
    _pageController.addListener((){
      setState(() {
        _activePage = (_pageController.page + 0.5).floor();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    AppState _appState = AppStateProvider.of(context);
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
        centerTitle: true,
        brightness: Brightness.dark,
        elevation: 0.0,
      ),
      body: new PageView(
        controller: _pageController,
        children: <Widget>[
          new PopularPage(appState: _appState,),
          new FavoritePage(appState: _appState,),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
          items: [
            new BottomNavigationBarItem(
              title: new Text('Popular'),
                icon: new Icon(Icons.movie)
            ),
            new BottomNavigationBarItem(
                title: new Text('Favorito'),
                icon: new Icon(Icons.favorite)),
          ],
         currentIndex: _activePage,
         onTap: (index) => onTap(index),
      ),
    );
  }


  void onTap(int index) {
    setState(() {
      _activePage = index;
    });
    _pageController.animateToPage(
        _activePage,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut
    );
  }
}
