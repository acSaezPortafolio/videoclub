class Movie {
  final int id;
  final String title;
  final String overView;
  final String posterPath;
  final String backdropPath;
  final double voteAverage;

  Movie( {
    this.id,
    this.title,
    this.overView,
    this.posterPath,
    this.backdropPath,
    this.voteAverage,
  });


  Movie.fromJson(Map<String, dynamic> json) :
    id = json['id'],
    title = json['title'],
    overView = json['overview'],
    posterPath = json['poster_path'],
    backdropPath = json['backdrop_path'],
    voteAverage = json['vote_average'].toDouble();

}