import 'package:flutter/material.dart';
import 'package:videoclub/Data/AppState.dart';


//este widget provee una instancia AppState al resto del árbol de widgets
class AppStateProvider extends InheritedWidget{

  final AppState appState;
  final Widget child;


  AppStateProvider({
    Key key,
    @required this.appState,
    @required this.child,
  }) : assert(appState != null),
       assert(child != null),
       super(key: key, child: child);


  static AppState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(AppStateProvider)
        as AppStateProvider).appState;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }

}