import 'package:videoclub/Data/Movie.dart';
import 'package:videoclub/Tools/api_key.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiMovie {

  static String apiAuth = "https://api.themoviedb.org/3/movie/76341?api_key=$key";

  static String popularUrl(int page) => 'https://api.themoviedb.org/3/movie/popular?api_key=$key&page=$page&language=es-ES';

  static String movieImgUrl(String path) => 'https://image.tmdb.org/t/p/w500$path';

  static String movieUrl(String movieId) => 'https://api.themoviedb.org/3/movie/$movieId?api_key=$key&language=es-ES';

  static Future<List<Movie>> getMovies(int page) async {
    http.Response response = await http.get(popularUrl(page));

    Map<String, dynamic> decoded = json.decode(response.body);

    List<dynamic> list = decoded['results'];

    List<Movie> movies = list.map((movie) {
      return Movie.fromJson(movie);
    }).toList();

    return movies;
  }
}