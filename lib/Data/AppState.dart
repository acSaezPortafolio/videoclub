import 'package:videoclub/Data/Movie.dart';

class AppState {
  List<int> favorites = [];
  List<Movie> movies = [];

  int lastpage = 0;

  addFavorite(int idMovie) {
    if (!favorites.contains(idMovie)) favorites.add(idMovie);
  }

  removeFavorite(int idMovie) {
    if (favorites.contains(idMovie)) favorites.remove(idMovie);
  }

  bool isFavorite(int idMovie) {
    return favorites.contains(idMovie);
  }
}