import 'package:flutter/material.dart';
import 'package:videoclub/Data/AppStateProvider.dart';
import 'package:videoclub/Data/Movie.dart';
import 'package:videoclub/Screens/movie_screen.dart';
import 'package:videoclub/Utils/api.dart';

class MovieCard extends StatelessWidget {
  final Movie movie;

  final String callerPage;

  MovieCard({@required this.movie, @required this.callerPage});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 5.0,
        vertical: 2.0,
      ),
      child: GestureDetector(
        onTap: () {
          _onTapWidget(context, movie);
        },
        child: new Card(
          elevation: 3.0,
          child: new Row(
            children: <Widget>[
              new Hero(
                  tag: "$callerPage${movie.id}",
                  child: new Container(
                    width: 120.0,
                    height: 170.0,
                    decoration: new BoxDecoration(
                      borderRadius:
                        new BorderRadius.horizontal(left: Radius.circular(5.0)),
                      image: new DecorationImage(
                          image: NetworkImage("${ApiMovie.movieImgUrl(movie.posterPath)}"),
                          fit: BoxFit.cover,
                      ),
                    ),
                  ),
              ),
              new Expanded(
                child: new Container(
                  height: 170.0,
                  child: new Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(movie.title,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                        new SizedBox(
                          height: 15.0,
                        ),
                        new Text(movie.overView,
                          style: Theme.of(context).textTheme.body1,
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                        ),
                        new Expanded(child: Container()),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  _onTapWidget(BuildContext context, Movie movie) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return MovieScreen(
          movie: movie,
          appState: AppStateProvider.of(context),
          callerPage: callerPage,
        );
      })
    );
  }
}
