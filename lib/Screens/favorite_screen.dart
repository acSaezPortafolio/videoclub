import 'package:flutter/material.dart';
import 'package:videoclub/Data/AppState.dart';
import 'package:videoclub/Widgets/MovieCard.dart';

class FavoritePage extends StatefulWidget {

  AppState appState;

  FavoritePage({@required this.appState}) : assert(appState != null);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: widget.appState.movies
      .where((movie) => widget.appState.isFavorite(movie.id))
      .map((movie) {
        return new MovieCard(
          movie: movie,
          callerPage: "favorite",
        );
      }
      ).toList(),
    );
  }
}
